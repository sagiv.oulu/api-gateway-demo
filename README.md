# api-gateway-demo

Demo nginx api gateway that can:

- redirect unauthenticated calls to oidc provider
- serve static files
- redirect authenticated calls to api service

## demo containers

### api

The api container is a stand in for your api server. The echo docker image is used, that returns the details of every requested directed to it (including headers, cookies...)

### Keycloak

An authentication provider to the gateway service. It is able to manage local users / sync users from other places.

### postgres

A database used by keycloak.

### gateway

An nginx container that all traffic goes through on port 80. the gateway basic logic is:

- /auth/\* -> redirected to keycloak
- /api/\* -> checks that the user is authenticated, & if so proxies to the api service.

  Requests with a valid access token in the authorization header, or requests with a session of an authenticated user are concidered authenticated.

  Unauthenticated requests are not redirected, but returned a 403 response

- /\* -> Checks that the request has a valid session.

  If the session is valid the request serves the files in `/static` folder.

  Unauthenticated requests are redirected to keycloak login form.
