import os
import re

# Example config:
# /auth ;           ;proxy http://keycloak.com:80 ; proxy_buffer_size 128k; proxy_buffers 4 256k;
# /users;verify-auth;proxy http://users.api.com:80;
# /calls;verify-auth;proxy http://calls.api.com:80;
# /lines;verify-auth;proxy http://lines.api.com:80;
# /     ;auth		;files /static                ;

LOCATIONS_DIR = r'/usr/local/openresty/nginx/conf/sites/locations'
config = os.getenv('CONFIG', '')

# Extract non empty line from config
lines = config.splitlines()
lines = [line.strip() for line in lines]
lines = [line for line in lines if line]

locations = []

for index, line in enumerate(lines):
    # Parse the line using the standard format
    [(uri, auth, destination_type, destination_address, custom_settings)] = re.findall(
        r'^\s*(.*?)\s*;\s*(.*?)\s*;\s*(.*?)\s+(.*?)\s*;(.*?)$',
        line)

    custom_settings = [f'{s.strip()};' for s in custom_settings.split(';') if s.strip()]

    location_props = custom_settings
    if auth:
        location_props.append(f'access_by_lua_file lua/{auth}.lua;')
        location_props.append('')

    if destination_type.lower() == 'proxy':
        location_props.append(f'proxy_set_header Host $host;')
        location_props.append(f'proxy_set_header X-Real-IP $remote_addr;')
        location_props.append(f'proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;')
        location_props.append(f'proxy_set_header X-Forwarded-Proto $scheme;')
        location_props.append(f'proxy_pass {destination_address};')
    elif destination_type.lower() == 'files':
        location_props.append(f'root {destination_address};')
        location_props.append(f'index index.html;')
        location_props.append(f'try_files $uri $uri.html $uri/ =404;')
    else:
        raise ValueError(f'Invalid destination type: {destination_type}')

    location = f'''
location {uri} {{
  {"""
  """.join(location_props)}
}}
'''
    locations.append(location)


os.makedirs(LOCATIONS_DIR)
for index, location in enumerate(locations):
    print(location)
    with open(os.path.join(LOCATIONS_DIR, f'{index}.conf'), 'w') as f:
        f.write(location)
