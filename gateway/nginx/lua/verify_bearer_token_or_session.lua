local verifiy_token_opts = {
  discovery = os.getenv("OID_DISCOVERY"),
  token_signing_alg_values_expected = { "RS256" },
  accept_unsupported_alg = false,
  jwk_expires_in = 24 * 60 * 60,
}

local verify_session_opts = {
  redirect_uri_path = os.getenv("OID_REDIRECT_PATH") or "/redirect_uri",
  discovery = os.getenv("OID_DISCOVERY"),
  client_id = os.getenv("OID_CLIENT_ID"),
  client_secret = os.getenv("OID_CLIENT_SECRET") or "notused",
  token_endpoint_auth_method = os.getenv("OIDC_AUTH_METHOD") or "client_secret_basic",
  renew_access_token_on_expiry = os.getenv("OIDC_RENEW_ACCESS_TOKEN_ON_EXPIERY") ~= "false",
  scope = os.getenv("OIDC_AUTH_SCOPE") or "openid",
  iat_slack = 600,
}

local token_res, token_err = require("resty.openidc").bearer_jwt_verify(verifiy_token_opts)
local session_res, session_err, _target, session = require("resty.openidc").authenticate(verify_session_opts, nil, "pass")

local token_verification_failed = token_err or not token_res
local session_verification_failed = session_err or not session_res

if token_verification_failed and session_verification_failed then
  ngx.status = 403
  ngx.header.content_type = 'text/html'

  ngx.say("Forbidden brearer token: " .. token_err)
  ngx.exit(ngx.HTTP_FORBIDDEN)
end

if not session_verification_failed and session then
  ngx.req.set_header("Authorization", "Bearer "..session.data.access_token)
end

ngx.log(ngx.INFO, "Identity verification successful")
