local opts = {
  discovery = os.getenv("OID_DISCOVERY"),
  token_signing_alg_values_expected = { "RS256" },
  accept_unsupported_alg = false,
  jwk_expires_in = 24 * 60 * 60,
}

-- call authenticate for OpenID Connect user authentication
local res, err = require("resty.openidc").bearer_jwt_verify(opts)

ngx.log(ngx.INFO, tostring(res))
ngx.log(ngx.INFO, tostring(err))

if err or not res then
  ngx.status = 403
  ngx.header.content_type = 'text/html';

  ngx.say("Forbidden: " .. err)
  ngx.exit(ngx.HTTP_FORBIDDEN)
end

ngx.log(ngx.INFO, "jwt token verification successful")
