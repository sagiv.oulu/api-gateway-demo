import requests
from pprint import pprint

gateway_host = 'localhost'

# master is the default realm
realm = 'master'

# Contact the developers to get hte client id they selected
client_id = 'sample-api'

username = 'admin'
password = 'admin'

url = f'http://{gateway_host}/auth/realms/{realm}/protocol/openid-connect/token'

response = requests.post(url, data={
    'grant_type': 'password',
    'username': username,
    'password': password,
    'client_id': client_id
})

print(response)
response.raise_for_status()

content = response.json()
pprint(content)

access_token = content.get('access_token')

print(f'access_token: {access_token}')
