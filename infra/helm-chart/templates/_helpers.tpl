{{- define "helm-chart.vars" -}}
{{- printf (and .Values.gateway.image.credentials.username .Values.gateway.image.credentials.password .Values.gateway.image.credentials.registry .Values.gateway.image.credentials.email) }}
{{- end }}

{{- define "globals.pullUsingCredentials" -}}
{{- if (and .Values.gateway.image.credentials.username .Values.gateway.image.credentials.password .Values.gateway.image.credentials.registry .Values.gateway.image.credentials.email) -}}
true
{{- else -}}
false
{{- end -}}
{{- end -}}