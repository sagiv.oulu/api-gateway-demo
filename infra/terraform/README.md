# Terraform deployment

This dir contains deployment files for the Azure Kubernetes Cluster that the application runs on. for now, this needs to be deployed manually by the user. in order to deploy you must:
## Initial setup on new workstation
1) install terraform 0.14
2) install azure cli
3) create an access token for your gitlab user
4) run the command:
```bash
terraform init 
    -backend-config="username=GITLAB_USERNAME" 
    -backend-config="password=ACCESS_TOKEN"
```

## Steps for deployment
1) Login to azure by running the command
    ```sh
    az login
    ```
    If your computer does not have a browser / the browser does not open automatically run:
    ```sh
    az login --use-device-code
    ```
    this will print a URL you need to browse to & a code to enter there.
2) Run the command:
    ```sh
    terraform plan
    ```
    This command will show you all the changes you are about to perform.
3) Run:
    ```sh
    terraform apply
    ``` 
    to run the changes. this will show you again the plan, and prompt you to answer `yes` if you approve the changes.
4) Wait until the command completes successfuly.
    
    Be patient... good thing happen to those who wait... and with Azure there is A LOT of waiting... :(

## Configuring kubectl to connect to k8s cluster
After applying the terraform config there will be an output called ```kube_config```, with the following content (the XXX will contain actual sensitive data about the cluster):
```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: XXX
    server: https://XXX
  name: XXX
contexts:
- context:
    cluster: XXX
    user: XXX
  name: XXX
current-context: XXX
kind: Config
preferences: {}
users:
- name: XXX
  user:
    client-certificate-data: XXX
    client-key-data: XXX
    token: XXX
```
Copy the content and write it into your kube config file. The default linux path is ```$HOME/.kube/config```

## Visualizing deployment
The terraform deployment can be automatically converted into an infrastructure diagram, such as:

![simple graph](./docs/terraform_graph_simple.png)

This is generated using [terraform-graph-beautifier](https://github.com/pcasteran/terraform-graph-beautifier), by running the command in this directory:
```bash
terraform graph | terraform-graph-beautifier \
    --exclude="module.root.provider" \
    --exclude="module.root.var" \
    --exclude="module.root.local" \
    --exclude="module.root.output" \
    --output-type=cyto-html \
    --embed-modules=true > terraform-graph.html
```

Note that you can create a more detailed graph that contains more terraform details such for example:
![full graph](./docs/terraform_graph_full.png)
This is generated with the command:
```bash
terraform graph | terraform-graph-beautifier \
    --exclude="module.root.provider" \
    --output-type=cyto-html \
    --embed-modules=true > terraform-graph.html
```

Note that this graph might be a bit over populated with tecnical information, but it helps visualize the terraform file and show problems with it (for example unused variables in the right bottom corner)